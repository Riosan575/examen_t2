﻿using Examen_T2.DB;
using Examen_T2.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Examen_T2.Controllers
{
    public class CapturarController : Controller
    {
        private PokemonContext context;
        public CapturarController(PokemonContext context)
        {
            this.context = context;
        }
        public IActionResult Index()
        {
            var user = GetLoggedUser();
            ViewBag.Capturas = context.Capturas.
            Include("Pokemons").Include("tipoPokemon").Where(o => o.UsuarioId == user.Id).ToList();
            ViewBag.Tipos = context.TipoPokemons;
            return View();
        }
        [HttpPost]
        public IActionResult Capturar(Captura capturar)
        {
            var user = GetLoggedUser();
            var fecha = DateTime.Now;

            if (ModelState.IsValid)
            {
                context.Capturas.Add(new Captura { UsuarioId = user.Id, IdPokemon = capturar.IdPokemon, IdTipo = capturar.IdTipo, Fecha = fecha });
                context.SaveChanges();
                return RedirectToAction("Index", "Capturar", new { id = capturar.IdPokemon, capturar.IdTipo});
            }
            return RedirectToAction("Index");
        }
        public IActionResult Delete(int id)
        {
            var captura = context.Capturas.Where(o => o.Id == id).FirstOrDefault();
            context.Capturas.Remove(captura);
            context.SaveChanges();
            return RedirectToAction("Index");
        }
        private User GetLoggedUser()
        {
            var claim = HttpContext.User.Claims.First();
            string username = claim.Value;
            var user = context.Users.First(o => o.Usuario == username);

            return user;
        }
    }
}
