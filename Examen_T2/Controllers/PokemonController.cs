﻿using Examen_T2.DB;
using Examen_T2.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Examen_T2.Controllers
{
    public class PokemonController : Controller
    {
        private IWebHostEnvironment hosting;
        private PokemonContext context;

        public PokemonController(PokemonContext context, IWebHostEnvironment hosting)
        {
            this.hosting = hosting;
            this.context = context;
        }
        public IActionResult Index()
        {
            ViewBag.pokemones = context.Pokemons.Include("TipoPokemon").ToList();
            return View();
        }
        [HttpGet]
        public IActionResult Create()
        {
            ViewBag.Tipos = context.TipoPokemons.ToList();
            return View(new Pokemon());
        }
        [HttpPost]
        public IActionResult Create(Pokemon pokemon, List<IFormFile> files, string nombre)
        {
            var nombres = context.Pokemons.ToList();
            foreach (var item in nombres)
            {
                if (item.Nombre.Equals(nombre)) { 

                    ModelState.AddModelError("Nombre", "El nombre ya existe, ingrese otro nombre");
                }
            }
            pokemon.Imagen = SaveFile(files[0]);
            context.Pokemons.Add(pokemon);
            context.SaveChanges();
            return RedirectToAction("Index");
        }
        private string SaveFile(IFormFile file)
        {
            string relativePath = "";

            if (file.Length > 0 && file.ContentType == "image/png")
            {
                relativePath = Path.Combine("files", file.FileName);
                var filePath = Path.Combine(hosting.WebRootPath, relativePath);
                var stream = new FileStream(filePath, FileMode.Create);
                file.CopyTo(stream);
                stream.Close();
            }

            return "/" + relativePath.Replace('\\', '/');
        }

    }
}
