﻿using Examen_T2.DB.Mapping;
using Examen_T2.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Examen_T2.DB
{
    public class PokemonContext: DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Pokemon> Pokemons { get; set; }
        public DbSet<Captura> Capturas { get; set; }
        public DbSet<TipoPokemon> TipoPokemons { get; set; }

        public PokemonContext(DbContextOptions<PokemonContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new UserMap());
            modelBuilder.ApplyConfiguration(new PokemonMap());
            modelBuilder.ApplyConfiguration(new CapturaMap());
            modelBuilder.ApplyConfiguration(new TipoPokemonMap());
        }
    }
}
