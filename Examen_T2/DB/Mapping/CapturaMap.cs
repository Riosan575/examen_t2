﻿using Examen_T2.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Examen_T2.DB.Mapping
{
    public class CapturaMap : IEntityTypeConfiguration<Captura>
    {
        public void Configure(EntityTypeBuilder<Captura> builder)
        {
            builder.ToTable("Captura");
            builder.HasKey(o => o.Id);

            builder.HasOne(o => o.Pokemons)
                .WithMany()
                .HasForeignKey(o => o.IdPokemon);

            builder.HasOne(o => o.tipoPokemon)
               .WithMany()
               .HasForeignKey(o => o.IdTipo);

        }
    }
}
