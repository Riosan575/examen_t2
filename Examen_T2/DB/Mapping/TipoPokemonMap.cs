﻿using Examen_T2.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Examen_T2.DB.Mapping
{
    public class TipoPokemonMap : IEntityTypeConfiguration<TipoPokemon>
    {
        public void Configure(EntityTypeBuilder<TipoPokemon> builder)
        {
            builder.ToTable("TipoPokemon");
            builder.HasKey(o => o.Id);

            //builder.HasMany(o => o.Books)
            //    .WithOne(o => o.Autor)
            //    .HasForeignKey(o => o.AutorId);
        }
    }
}
