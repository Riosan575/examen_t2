﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Examen_T2.Models
{
    public class Captura
    {
        public int Id { get; set; }
        public int IdPokemon { get; set; }  
        public DateTime Fecha { get; set; }
        public int UsuarioId { get; set; }
        public int IdTipo { get; set; }
        public Pokemon Pokemons { get; set; }
        public TipoPokemon tipoPokemon { get; set; }
    }
}
