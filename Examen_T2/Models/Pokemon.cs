﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Examen_T2.Models
{
    public class Pokemon
    {        
        public int Id { get; set; }
        [Required(ErrorMessage = "La casilla nombre es obligatoria")]
        public string Nombre { get; set; }
        [Required(ErrorMessage = "La casilla tipo es obligatoria")]
        public int IdTipo { get; set; }
        [Required(ErrorMessage = "La casilla imagen es obligatoria")]
        public string Imagen { get; set; }
        public TipoPokemon TipoPokemon { get; set; }
    }
}
