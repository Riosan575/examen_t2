﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Examen_T2.Models
{
    public class TipoPokemon
    {
        public int Id { get; set; } 
        public string TipoPokemons { get; set; }
    }
}
